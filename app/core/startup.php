<?php

// Create connection
$conn = new mysqli($host, $user, $password, $database);

/* изменение набора символов на utf8 */
$conn->set_charset("utf8");

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

session_start();


?>
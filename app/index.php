<?php
include 'config/db.php';
include 'core/startup.php';
include 'core/auth.php';

?>
<!doctype html>
<html>
<head>
	<meta charset="utf8"/>
	<title>Chat</title>

	<link rel="stylesheet" href="style.css"/>
</head>
<body>
<div id="wrapper">

	<?php if ($g_user): ?>
		<h1>Hello, <?php echo $g_user['username']; ?>! 
			<form id="logout" method="post">
				<button type="submit" name="logout">Logout</button>
			</form>
		</h1>
	<?php else: ?>
		<h1>Chat</h1>
	<?php endif; ?>

	<?php if (!$g_user): ?> 
		<form id="login" method="POST" action="auth/login.php">
			<input type="text" name="username" placeholder="Enter your name"/>
			<br/>
			<input type="password" name="password" placeholder="Enter your pass"/>
			<br/>
			<button type="submit" name="login">Login</button>
		</form>
	<?php endif; ?>


	



</div>
<script type="text/javascript" src="script.js"></script>
</body>
</html>
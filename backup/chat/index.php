<?php
include '../db.php';

session_start();

$user = null;

include 'core/request.php';

// Существует ли в сессии пользователь
if (isset($_SESSION['user'])) {
	$user = $_SESSION['user'];
}

?>
<!doctype html>
<html>
<head>
	<meta charset="utf8"/>
	<title>Chat</title>

	<link rel="stylesheet" href="style.css"/>
</head>
<body>
<div id="wrapper">
	<?php if ($user): ?>
		<h1>Hello, <?php echo $user; ?>! 
			<form id="logout" method="post">
				<button type="submit" name="logout">Logout</button>
			</form>
		</h1>
	<?php else: ?>
		<h1>Chat</h1>
	<?php endif; ?>

	<?php if (!$user): ?> 
		<form id="login" method="POST" action="index.php">
			<input type="text" name="username" placeholder="Enter your name"/>
			<button type="submit" name="login">Go</button>
		</form>
	<?php endif; ?>

	<form 
		id="message" 
		style="display: <?php echo $user ? "block" : "none"; ?>"
		action="index.php"
		method="post">
		<textarea name="text" placeholder="Enter your comment"></textarea>
		<button type="submit" name="send">Send</button>
	</form>

	<div id="message_list">
		<?php
			include 'partials/message_list.php';
		?>
	</div>

</div>
<script type="text/javascript" src="script.js"></script>
</body>
</html>
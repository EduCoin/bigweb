
<?php
	if(!isset($conn)) {
		include '../../db.php';
		session_start();
	}
	
	// формируем sql запрос
	$sql = "SELECT * FROM chat ORDER BY id DESC";
	// выполняем запрос
	$result = $conn->query($sql);

	// если есть записи в таблице
	if ($result->num_rows > 0) {
		// преобразовываем записи в ассоциативный массив row['<field>']
	    while($row = $result->fetch_assoc()) {
	    	if(isset($_SESSION['user']) && $row['username'] === $_SESSION['user']) {
		    	echo "<article class='current_user'>
						<h2>" . $row['username'] . "</h2>
						<p>" . $row['description'] . "</p>
					</article>";	
			} else {
				echo "<article>
					<h2>" . $row['username'] . "</h2>
					<p>" . $row['description'] . "</p>
				</article>";
			}
			echo "<div class='clearfix'></div>";
	    }
	} else {
	    echo "0 results";
	}

 ?>


<?php
$host = 'localhost'; // адрес сервера 
$database = 'project_test'; // имя базы данных
$user = 'root'; // имя пользователя
$password = ''; // пароль



// Create connection
$conn = new mysqli($host, $user, $password, $database);

/* изменение набора символов на utf8 */
if (!$conn->set_charset("utf8")) {
    printf("Ошибка при загрузке набора символов utf8: %s\n", $conn->error);
    exit();
} else {
    printf("Текущий набор символов: %s\n", $conn->character_set_name());
}

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

?>
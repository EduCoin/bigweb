<?php include("db.php"); ?>

<!doctype html>
<html>
<head>
<title>Work PHP</title>
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

<div id="wrapper">
	<?php include("partial/header.php"); ?> 

	<table>
	  <tr>
	    <th>ID</th>
	    <th>FIO</th>
	    <th>Email</th>
	    <th>Phone</th>
	    <th>Role</th>
	    <th>Options</th>
	  </tr>


  	<?php
  	// формируем sql запрос
	$sql = "SELECT * FROM users WHERE deleted=0";
	// выполняем запрос
	$result = $conn->query($sql);

	// если есть записи в таблице
	if ($result->num_rows > 0) {
		// преобразовываем записи в ассоциативный массив row['<field>']
	    while($row = $result->fetch_assoc()) {
			echo "<tr>";
				echo "<td>" . $row['id'] . "</td>";
				echo "<td>" . $row['fio'] . "</td>";
				echo "<td>" . $row['email'] . "</td>";
				echo "<td>" . $row['phone'] . "</td>";
				echo "<td>" . $row['role_id'] . "</td>";
				echo "<td><a href='user/edit.php?id=" . $row['id'] . "'>Edit</a> 
				<a href='user/delete.php?id=" . $row['id'] . "' class='btn-delete'>Delete</a></td>";
			echo "</tr>";
	    }
	} else {
	    echo "0 results";
	}

  	?>	  
	</table>
</div>



<script type="text/javascript" src="/js/script.js"></script>
</body>
</html>



function btnHandlerDeleteUser(e) {
	e.preventDefault();
	console.log(e)
	var answer = confirm("Вы точно хотите удалить запись?");
	if(answer) {
		location.href = e.target.href;
	}
}

var btnsDelete = document.querySelectorAll(".btn-delete");

for (var i = 0; i < btnsDelete.length; i++){
	btnsDelete[i].onclick = btnHandlerDeleteUser;
}
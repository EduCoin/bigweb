<?php include("../db.php"); ?>

<!doctype html>
<html>
<head>
<title>Create new user</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>

<div id="wrapper">
	<?php include("../partial/header.php"); ?> 

	<?php /*
		1. html сделать поля для ввода
		2. считывать с полей информацию
		3. добавлять данные в какой то массив
		4. отправить его на сервер
		5. настройки к конекту к бд
		6. валидация полученных на сервер данных
	*/ ?>
    <h2>Create</h2>
    <form class="form" method="post" action="add.php">
    	<p>
        	<label for="username">Username</label>
        	<input type = "text" id = "username" name = "username" placeholder="Jason"/>
    	</p>
        <p>
        	<label for = "email">Email</label>
        	<input class = "form-data-parse" type = "email" id = "email" name = "email" placeholder="jason@example.com"/>
    	</p>
    	<p>
        	<label for = "phone">Phone</label>
        	<input class = "form-data-parse" type = "text" id = "phone" name = "phone" placeholder="jason@example.com"/>
    	</p>
    	<p>
        	<label for = "fio">FIO</label>
        	<input class = "form-data-parse" type = "text" id = "fio" name = "fio" placeholder="jason@example.com"/>
    	</p>
    	<p>
        	<label for = "role">Role</label>
        	<input class = "form-data-parse" type = "text" id = "role" name = "role" placeholder="jason@example.com"/>
    	</p>
    	<p>
        	<label for = "password">Password</label>
        	<input type= "password" id = "password" name = "password" placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;"/>
    	</p>
    	<p>
        	<button type="submit">Register</button>
    	</p>
    </form>

	
</div>

</body>
</html>



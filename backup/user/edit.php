<?php include("../db.php"); ?>

<!doctype html>
<html>
<head>
    <title>Edit user</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>
<div id="wrapper">
	<?php include("../partial/header.php"); ?> 

    <?php 
        $sql = "SELECT * FROM users WHERE id=" . $_GET['id']; 
        $result = $conn->query($sql);
        $user = $result->fetch_assoc();
    ?>


    <h2>Edit</h2>
    <form class="form" method="post" action="update.php">
        <input type="hidden" name="id" value="<?php echo $user['id']; ?>">
    	<p>
        	<label for="username">Username</label>
        
        	<input type = "text" id = "username" name = "username" placeholder="Jason" value="<?php echo $user['username']; ?>"/>
    	</p>
        <p>
        	<label for = "email">Email</label>
        	<input 
                class = "form-data-parse" 
                type = "email" id = "email" name = "email" placeholder="jason@example.com"  
                value="<?php echo $user['email']; ?>"/>
    	</p>
    	<p>
        	<label for = "phone">Phone</label>
        	<input 
                class="form-data-parse" 
                type="text" 
                id="phone" 
                name="phone" 
                placeholder="jason@example.com" 
                value="<?php echo $user['phone']; ?>" />
    	</p>
    	<p>
        	<label for="fio">FIO</label>
        	<input 
                class="form-data-parse" 
                type="text" 
                id="fio" 
                name="fio"
                value="<?php echo $user['fio']; ?>"  
                placeholder="jason@example.com"/>
    	</p>
    	<p>
        	<label for = "role">Role</label>
        	<input value="<?php echo $user['role_id']; ?>"  class = "form-data-parse" type = "text" id = "role" name = "role" placeholder="jason@example.com"/>
    	</p>
    	<p>
        	<button type="submit">Update</button>
    	</p>
    </form>
</div>
</body>
</html>


